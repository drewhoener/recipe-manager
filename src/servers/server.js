import express from "express";
import webpack from "webpack";
import webpackDevMiddleware from "webpack-dev-middleware";
import path from "path";
import logger from '../js/logging';
import * as db from "../database";
import {get_recipes} from "../database";

const log = logger.extend('server');
const app = express(), STATIC = __dirname, STATIC_HTML = path.join(STATIC, 'index.html');
const expressStaticGzip = require('express-static-gzip');
const config = require('../../webpack.config.js');
const webpackCompiler = webpack(config);
const bodyParser = require('body-parser');


log('Starting up');
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {    //Recompile on local changes so we don't have to rebuild
    log('Using Webpack rebuild');
    app.use(webpackDevMiddleware(webpackCompiler, {
        quiet: true,
        stats: {
            assets: false,
            modules: false,
            chunks: false,
            children: false,
            hash: false,
            entrypoints: false,
            version: false,
        },
        publicPath: config.output.publicPath
    }));
    //use default static files
}

//Serve static html from dist directory
app.use(bodyParser.json());


app.post('/api/add_recipe', (req, res, next) => {
    const body = req.body;
    if (!body || !body.name || !body.ingredients || !body.instructions) {
        res.status(422).json({message: "Invalid recipe"});
        return;
    }
    db.add_recipe(body).then(result => {
        res.status(200).json(Object.assign({message: 'ok'}, {recipe: result}));
    }).catch(err => {
        res.status(500).json({message: err.message});
    });
});

app.get('/api/recipes', async (req, res, next) => {
    let recipes = await get_recipes();
    res.status(200).json({recipes: recipes});
});

//Respond to get requests, serve the base html file
app.get('*', expressStaticGzip(STATIC, {
    enableBrotli: true
}));

app.listen(3000, () => {
    db.connect();
    log("Express Server running on port 3000");
});
import {Checkbox, Label, Segment, Table} from "semantic-ui-react";
import React from "react";
import {connect} from "react-redux";

const IngredientTable = ({uid, array, type, filter, isMobile}) => {
    return (
        <Segment>
            <Label attached={'top'} content={`${type} Ingredients`}/>
            <Table unstackable fixed collapsing={isMobile}>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width={1}/>
                        <Table.HeaderCell width={10}>Ingredient</Table.HeaderCell>
                        <Table.HeaderCell width={2}>Qty</Table.HeaderCell>
                        <Table.HeaderCell width={3}>Unit</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {
                        array.filter(filter).map((i, index) => (
                            <Table.Row key={`${uid}-${index}`}>
                                <Table.Cell><Checkbox/></Table.Cell>
                                <Table.Cell>{i.name}</Table.Cell>
                                <Table.Cell>{i.qty}</Table.Cell>
                                <Table.Cell>{i.unit}</Table.Cell>
                            </Table.Row>
                        ))
                    }
                </Table.Body>
            </Table>
        </Segment>
    );
};

const mapStateToProps = state => ({
    isMobile: state.width.isMobile
});

export default connect(
    mapStateToProps,
    null
)(IngredientTable);
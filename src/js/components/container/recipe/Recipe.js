import {Grid, Label, Segment} from "semantic-ui-react";
import React from "react";
import {connect} from "react-redux";
import IngredientTable from "./IngredientTable";
import {setCurrentInstruction} from "../../../store/actions/recipes";
import InstructionPagination from "./InstructionPagination";
import RecipePageButtons from "./RecipePageButtons";
import InstructionAccordion from "./InstructionAccordion";

const styleProps = {
    display: 'inline-block',
    margin: '0 auto'
};

const Recipe = ({recipe, curInstruction, isMobile}) => {
    return (
        <Segment.Group>
            <Segment padded={true}>
                <Label attached={'top'} content={recipe.name} size={'big'}/>
            </Segment>
            <Segment>
                <Grid divided stackable columns={'equal'}>
                    <Grid.Row>
                        <Grid.Column>
                            <IngredientTable uid={recipe.uid} array={recipe.ingredients} filter={(r) => !r.wet}
                                             type={'Dry'}/>
                        </Grid.Column>
                        <Grid.Column>
                            <IngredientTable uid={recipe.uid} array={recipe.ingredients} filter={(r) => r.wet}
                                             type={'Wet'}/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
            <Segment>
                <Label attached={'top'} content={'Instructions'}/>
                <Grid divided={!isMobile}>
                    <Grid.Column width={14}>
                        <RecipePageButtons numInstructions={recipe.instructions.length}/>
                        <InstructionAccordion instructions={recipe.instructions}/>
                    </Grid.Column>
                    <Grid.Column width={2}>
                        <InstructionPagination style={styleProps} curInstruction={curInstruction + 1}
                                               numInstructions={recipe.instructions.length}/>
                    </Grid.Column>
                </Grid>
            </Segment>
        </Segment.Group>
    )
};

const mapStateToProps = state => ({
    recipe: state.recipes.recipes.find(r => r.uid === state.curRecipe),
    curInstruction: state.curInstruction,
    isMobile: state.width.isMobile
});

const mapDispatchToProps = dispatch => ({
    setCurInstruction: index => dispatch(setCurrentInstruction(index))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Recipe);
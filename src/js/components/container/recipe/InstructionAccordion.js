import {Accordion, Icon} from "semantic-ui-react";
import React from "react";
import {connect} from "react-redux";
import {setCurrentInstruction} from "../../../store/actions/recipes";

const InstructionAccordion = ({instructions, curIndex, updateInstruction}) => {
    const onClick = (event, data) => updateInstruction(data.index);
    return (
        <Accordion fluid styled>
            {
                instructions.map((i, index) => (
                    <React.Fragment key={`instruction-${index}`}>
                        <Accordion.Title active={curIndex === index} index={index} onClick={onClick}>
                            <Icon name={'dropdown'}/>
                            {`Step ${index + 1}`}
                        </Accordion.Title>
                        <Accordion.Content active={curIndex === index}>
                            <p>
                                {i}
                            </p>
                        </Accordion.Content>
                    </React.Fragment>
                ))
            }
        </Accordion>
    )
};

const mapStateToProps = state => ({
    curIndex: state.curInstruction
});

const mapDispatchToProps = dispatch => ({
    updateInstruction: (index) => dispatch(setCurrentInstruction(index))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InstructionAccordion);
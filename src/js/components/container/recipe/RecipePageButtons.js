import {Button, Grid, Segment} from "semantic-ui-react";
import React from "react";
import {connect} from "react-redux";
import {setCurrentInstruction} from "../../../store/actions/recipes";

const RecipePageButtons = ({numInstructions, curInstruction, updateInstruction}) => {
    const prevInstruction = () => updateInstruction(curInstruction - 1 < 0 ? 0 : curInstruction - 1);
    const nextInstruction = () => updateInstruction(curInstruction + 1 >= numInstructions ? numInstructions - 1 : curInstruction + 1);

    return (
        <Segment>
            <Grid columns={'equal'}>
                <Grid.Column>
                    <Button type={'button'} icon={'left arrow'} labelPosition={'left'}
                            content={'Previous Step'} onClick={prevInstruction}/>
                </Grid.Column>
                <Grid.Column>
                    <Button floated={'right'} type={'button'} icon={'right arrow'} labelPosition={'right'}
                            content={'Next Step'} onClick={nextInstruction}/>
                </Grid.Column>
            </Grid>
        </Segment>
    );
};

const mapStateToProps = state => ({
    curInstruction: state.curInstruction
});

const mapDispatchToProps = dispatch => ({
    updateInstruction: (index) => dispatch(setCurrentInstruction(index))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RecipePageButtons);
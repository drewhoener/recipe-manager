import React from 'react';
import {Pagination} from "semantic-ui-react";
import {setCurrentInstruction} from "../../../store/actions/recipes";
import {connect} from "react-redux";

const InstructionPagination = ({style, curInstruction, numInstructions, updateInstruction}) => {
    const onClick = (event, data) => updateInstruction(data.activePage - 1);
    return (
        <Pagination floated={'right'} style={style} activePage={curInstruction} onPageChange={onClick}
                    prevItem={null}
                    nextItem={null}
                    totalPages={numInstructions}/>
    );
};

const mapDispatchToProps = dispatch => ({
    updateInstruction: (index) => dispatch(setCurrentInstruction(index))
});

export default connect(
    null,
    mapDispatchToProps
)(InstructionPagination);
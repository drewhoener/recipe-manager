import React from 'react';
import {Button, Modal} from "semantic-ui-react";
import {formSubmitRecipe, resetForm} from "../../../store/actions/form";
import {connect} from "react-redux";
import {setModalState} from "../../../store/actions/modal";
import AddFormBrains from "./AddFormBrains";

const AddRecipeModal = ({addModalOpen, setModalState, resetForm, submitForm}) => (
    <Modal size={'large'} open={addModalOpen} onClose={e => setModalState(e, false)} centered={false} closeIcon>
        <Modal.Header content={'Manually add a new Recipe'}/>
        <Modal.Content>
            <AddFormBrains/>
        </Modal.Content>
        <Modal.Actions>
            <Button negative icon={'close'} labelPosition={'right'} content={'Close'}
                    onClick={e => setModalState(e, false)} type={'button'}/>
            <Button positive icon='add' labelPosition='right' content='Add Recipe' type={'submit'}
                    onClick={submitForm}/>
            <Button negative icon={'refresh'} labelPosition={'right'} content={'Reset'} type={'button'}
                    onClick={resetForm}/>
        </Modal.Actions>
    </Modal>
);

const mapPropsToState = state => ({
    addModalOpen: state.addModalOpen
});

const mapDispatchToState = dispatch => ({
    submitForm: (event) => dispatch(formSubmitRecipe(event)),
    setModalState: (e, state) => dispatch(setModalState(e, state)),
    resetForm: () => dispatch(resetForm())
});

export default connect(
    mapPropsToState,
    mapDispatchToState
)(AddRecipeModal);
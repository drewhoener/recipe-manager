import React from "react";
import {Popup} from "semantic-ui-react";
import {evalError} from "../../../store/constants";

const ErrorPopup = ({children, message, position = 'top left'}) => {
    const disabled = (typeof message === 'boolean') ? message : !evalError(message);
    return (
        <Popup content={message} disabled={disabled} trigger={children} position={position}/>
    );
};

export default ErrorPopup;
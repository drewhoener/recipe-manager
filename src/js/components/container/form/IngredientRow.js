import React from 'react';
import {Form} from 'semantic-ui-react';
import {connect} from "react-redux";
import {changeAndValidateIngredientRow} from "../../../store/actions/form";
import ErrorPopup from "./ErrorPopup";
import {evalError} from "../../../store/constants";

const units = [
    'tsp',
    'tbsp',
    'fl oz',
    'cup',
    'pint',
    'quart',
    'gallon',
    'pound',
    'oz'
];

const IngredientRow = ({index, onChange, name, qty, unit, wet, nameErr, qtyErr, unitErr, wetErr}) => {

    const unit_options = [{key: `dropdown-unit-${index}-none`, text: '--', value: '--'}].concat(
        units.sort().map(val => {
            return {key: `dropdown-unit-${index}-${val}`, text: val, value: val};
        })
    );

    const wet_options = [
        {key: `dropdown-wet-${index}-none`, text: '--', value: -1},
        {key: `dropdown-wet-${index}-yes`, text: 'Wet', value: 1},
        {key: `dropdown-wet-${index}-no`, text: 'Dry', value: 0}
    ];

    return (
        <Form.Group>
            <ErrorPopup message={nameErr}>
                <Form.Input width={10} label={'Name'} name={'name'} placeholder={'Ingredient...'} onChange={onChange}
                            value={name} error={evalError(nameErr)} autoFocus/>
            </ErrorPopup>
            <ErrorPopup message={qtyErr}>
                <Form.Input key={`ingredient-row-${index}-qty`} width={2} label={'Qty.'} name={'qty'}
                            onChange={onChange} value={qty}
                            error={evalError(qtyErr)} autoFocus/>
            </ErrorPopup>
            <ErrorPopup message={unitErr}>
                <Form.Dropdown width={2} options={unit_options} label={'Unit'} name={'unit'} onChange={onChange}
                               value={unit} error={evalError(unitErr)}
                               search selection compact/>
            </ErrorPopup>
            <ErrorPopup message={wetErr} position={'top right'}>
                <Form.Dropdown width={2} options={wet_options} label={'Type'} name={'wet'} onChange={onChange}
                               value={wet}
                               search selection compact error={evalError(wetErr)}/>
            </ErrorPopup>
        </Form.Group>
    );
};

const mapStateToProps = (state, ownProps) => {
    const errors = state.form.ingredients.errors[ownProps.index];
    const values = state.form.ingredients.values[ownProps.index];
    return {
        ...values,
        nameErr: errors.name,
        qtyErr: errors.qty,
        unitErr: errors.unit,
        wetErr: errors.wet
    }
};

const mapDispatchToProps = (dispatch, ownProps) => ({
    onChange: (event, data) => {
        const {name, value} = data;
        dispatch(changeAndValidateIngredientRow(ownProps.index, name, value));
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(IngredientRow);

export {units};
import React from 'react';
import {Form} from 'semantic-ui-react'
import {changeAndValidateInstructionRow} from "../../../store/actions/form";
import {connect} from "react-redux";
import ErrorPopup from "./ErrorPopup";
import {evalError} from "../../../store/constants";

const InstructionRow = ({index, onChange, description, error}) => {
    return (
        <Form.Group>
            <Form.Input size={'mini'} width={1} name={'step'} onChange={onChange} value={index + 1} readOnly/>
            <ErrorPopup message={error}>
                <Form.TextArea rows={1} width={15} name={'description'} placeholder={'Crack an egg...'}
                               onChange={onChange}
                               value={description} error={evalError(error)} autoFocus/>
            </ErrorPopup>
        </Form.Group>
    )
};

const mapDispatchToProps = (dispatch, ownProps) => ({
    onChange: (event, data) => {
        const {value} = data;
        dispatch(changeAndValidateInstructionRow(ownProps.index, value));
    }
});

export default connect(
    null,
    mapDispatchToProps
)(InstructionRow);
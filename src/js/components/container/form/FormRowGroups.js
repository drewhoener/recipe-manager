import React from "react";
import {Button, Label, Segment} from 'semantic-ui-react';
import IngredientRow from "./IngredientRow";
import InstructionRow from "./InstructionRow";
import {
    addIngredientRow,
    addInstructionRow,
    removeIngredientRow,
    removeInstructionRow
} from "../../../store/actions/form";
import {connect} from "react-redux";

const FormRowGroups = ({onAdd, onRemove, ingredients, instructions}) => {
    const {onAddIngredient, onAddInstruction} = onAdd;
    const {removeIngredient, removeInstruction} = onRemove;
    return (
        <Segment.Group>
            <Segment content={'Ingredients'}/>
            <Segment.Group>
                {
                    ingredients.values.map((ingredient, index) => {
                        return (
                            <Segment key={`ingredient-segment-index-${index}`} padded>
                                <Label attached='top right' icon={'delete'}
                                       onClick={evt => removeIngredient(evt, index)}/>
                                <IngredientRow key={`ingredient-index-${index}`} index={index}/>
                            </Segment>
                        )
                    })
                }
                <Segment>
                    <Button type={'button'} onClick={evt => onAddIngredient(evt, ingredients.values.length)}
                            content={'Add Ingredient'}/>
                </Segment>
            </Segment.Group>
            <Segment content={'Instructions'}/>
            <Segment.Group>
                {
                    instructions.values.map((instruction, index) => {
                        return (
                            <Segment key={`instruction-segment-index-${index}`} padded>
                                <Label attached='top right' icon={'delete'}
                                       onClick={evt => removeInstruction(evt, index)}/>
                                <InstructionRow key={`instruction-index-${index}`} error={instructions.errors[index]}
                                                description={instruction} index={index}/>
                            </Segment>
                        )
                    })
                }
                <Segment>
                    <Button type={'button'} onClick={evt => {
                        onAddInstruction(evt, instructions.values.length)
                    }} content={'Add Instruction'}/>
                </Segment>
            </Segment.Group>
        </Segment.Group>
    );
};

const mapStateToProps = state => ({
    instructions: state.form.instructions,
    ingredients: state.form.ingredients,
});

const mapDispatchToProps = dispatch => ({
    onAdd: {
        onAddIngredient: (event, index) => dispatch(addIngredientRow(event, index)),
        onAddInstruction: (event, index) => dispatch(addInstructionRow(event, index)),
    },
    onRemove: {
        removeIngredient: (event, index) => dispatch(removeIngredientRow(event, index)),
        removeInstruction: (event, index) => dispatch(removeInstructionRow(event, index))
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FormRowGroups);
import React from "react";
import {connect} from 'react-redux';
import {Form, Message} from "semantic-ui-react";
import {changeAndValidateName} from "../../../store/actions/form";
import FormRowGroups from "./FormRowGroups";
import ErrorPopup from "./ErrorPopup";
import {evalError} from "../../../store/constants";
//TODO do some form validation
const AddFormBrains = ({onNameChange, name, nameError, formError, formValid}) => {
    return (
        <Form>
            <ErrorPopup message={nameError}>
                <Form.Input
                    label={'Recipe Name'} placeholder={'Enter recipe name'} error={evalError(nameError)}
                    onChange={event => onNameChange(event.target.value)} value={name} autoFocus
                />
            </ErrorPopup>
            <FormRowGroups/>
            <Message error visible={!formValid} header='Form Error' content={formError}/>
        </Form>
    )
};

const mapStateToProps = state => ({
    name: state.form.name,
    nameError: state.form.nameError,
    formError: state.form.formError,
    formValid: state.form.formValid
});

const mapDispatchToProps = dispatch => ({
    onNameChange: name => dispatch(changeAndValidateName(name))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddFormBrains);
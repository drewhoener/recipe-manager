import React from 'react';

const Conditional = ({children, visible}) => {
    return (
        visible &&
        {
            ...children
        }
    );
};

export default Conditional;
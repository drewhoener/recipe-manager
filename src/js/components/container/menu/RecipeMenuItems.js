import React from "react";
import {connect} from 'react-redux';
import {Button, Icon, Input, Menu} from "semantic-ui-react";
import {setCurrentRecipe} from "../../../store/actions";
import RecipeInstanceItem from "../../presentation/RecipeInstanceItem";
import {setModalState} from "../../../store/actions/modal";

const RecipeMenuItems = ({sidebarOpen, curWidth, inverted, recipes, curItem, setCurItem, printState, setModalState}) => {
    let buttonStyle = {};
    if(curWidth <= 1440 && !sidebarOpen)
        buttonStyle.float = 'left';
    let sidebarFloat = sidebarOpen || curWidth > 1100;

    return (
    <>
        <Menu.Item>
            <Input placeholder='Search...' inverted={inverted}/>
        </Menu.Item>
        <Menu.Item>
            Recipes
            <Menu.Menu>
                {
                    recipes.map(item => {
                        let options = {
                            name: item.name,
                            uid: item.uid,
                            curItem,
                            setCurItem
                        };
                        return (
                            <RecipeInstanceItem key={item.uid} {...options}/>
                        );
                    })
                }
            </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
            <div className={'paddy_boi'}>
                <Button icon labelPosition={'right'} inverted={inverted} fluid={sidebarFloat}
                        onClick={e => setModalState(e, true)}>
                    <div style={buttonStyle}>Add Recipe</div>
                    <Icon name={'plus'}/>
                </Button>
            </div>
            <div className={'paddy_boi'}>
                <Button icon labelPosition={'right'} inverted={inverted} fluid={sidebarFloat}>
                    <div style={buttonStyle}>Import Recipe</div>
                    <Icon name={'right arrow'}/>
                </Button>
            </div>
            <div className={'paddy_boi'}>
                <Button icon labelPosition={'right'} inverted={inverted} fluid={sidebarFloat} onClick={printState}>
                    <div style={buttonStyle}>Log State</div>
                    <Icon name={'print'}/>
                </Button>
            </div>
        </Menu.Item>
    </>
);};

const mapStateToProps = state => ({
    recipes: state.recipes.recipes,
    curItem: state.curRecipe,
    curWidth: state.width.curWidth,
    sidebarOpen: state.sidebarVisible,
    printState: () => console.log(state)
});

const mapDispatchToProps = dispatch => ({
    setCurItem: uid => dispatch(setCurrentRecipe(uid)),
    setModalState: (e, state) => dispatch(setModalState(e, state))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RecipeMenuItems);
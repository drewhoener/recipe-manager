import React from 'react';
import {Menu, Responsive} from "semantic-ui-react";
import RecipeMenuItems from "./RecipeMenuItems";

const RecipeMenu = () => (
    <Responsive as={'div'} {...Responsive.onlyComputer}>
        <Menu stackable fluid vertical>
            <RecipeMenuItems inverted={false}/>
        </Menu>
    </Responsive>
);

export default RecipeMenu;
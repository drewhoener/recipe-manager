import React from "react";
import {connect} from 'react-redux';
import {toggleSidebar} from "../../../store/actions";
import {Icon, Menu, Responsive} from "semantic-ui-react";

const OpenSidebarMenuItem = ({onClick}) => (
    <Responsive as={Menu.Item} maxWidth={Responsive.onlyTablet.maxWidth}
        name='sidebar-open'
        onClick={onClick}
        icon
    >
        <Icon name={'content'}/>
    </Responsive>
);

const mapDispatchToProps = dispatch => ({
    onClick: e => dispatch(toggleSidebar(true))
});

export default connect(
    null,
    mapDispatchToProps
)(OpenSidebarMenuItem);
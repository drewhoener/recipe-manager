import React from "react";
import {connect} from 'react-redux';
import {Menu, Responsive, Sidebar} from "semantic-ui-react";
import RecipeMenuItems from "./RecipeMenuItems";
import {toggleSidebar} from "../../../store/actions";
import MainMenu from "../../presentation/MainMenu";

const RecipeHiddenSidebar = ({sidebarVisible, onSidebarHide}) => (
    <div>
        <div>
            <MainMenu/>
        </div>
        <Sidebar
            as={Menu}
            animation='overlay'
            icon='labeled'
            inverted
            onHide={onSidebarHide}
            vertical
            visible={sidebarVisible}
            width='wide'
        >
            <RecipeMenuItems inverted={true}/>
        </Sidebar>
    </div>
);

const mapStateToProps = state => ({
    sidebarVisible: state.sidebarVisible
});

const mapDispatchToProps = dispatch => ({
    onSidebarHide: () => dispatch(toggleSidebar(false))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RecipeHiddenSidebar);

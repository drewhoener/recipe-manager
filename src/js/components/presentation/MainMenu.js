import React from 'react';
import {Menu} from "semantic-ui-react";
import OpenSidebarMenuItem from "../container/menu/OpenSidebarMenuItem";

const MainMenu = () => (
    <Menu fixed={'top'}>
        <OpenSidebarMenuItem/>
        <Menu.Item name='reviews'>
            Reviews
        </Menu.Item>
    </Menu>
);

export default MainMenu;
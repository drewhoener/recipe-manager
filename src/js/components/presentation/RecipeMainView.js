import {Grid, Responsive} from "semantic-ui-react";
import PushableView from "./PushableView";
import React from "react";
import {connect} from 'react-redux';
import {updateScreenWidth} from "../../store/actions";
import RecipeMenu from "../container/menu/RecipeMenu";
import AddRecipeModal from "../container/form/AddRecipeModal";
import Recipe from "../container/recipe/Recipe";
import Conditional from "../container/Conditional";

const RecipeMainView = ({isMobile, updateScreen, curRecipe}) => (
    <Responsive as={PushableView} onUpdate={updateScreen}>
        <AddRecipeModal/>
        <div className={'full-height padded-top'}>
            <Grid stretched divided={!isMobile}>
                <Grid.Row className={'full-height'}>
                    <Grid.Column width={3}>
                        <RecipeMenu/>
                    </Grid.Column>
                    <Grid.Column width={isMobile ? 16 : 13} className={'scrollable'}>
                        <Conditional visible={curRecipe !== ''}>
                            <Recipe/>
                        </Conditional>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </div>
    </Responsive>
);

const mapStateToProps = state => ({
    isMobile: state.width.isMobile,
    curRecipe: state.curRecipe
});

const mapDispatchToProps = dispatch => ({
    updateScreen: (event, data) => dispatch(updateScreenWidth(event, data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RecipeMainView);



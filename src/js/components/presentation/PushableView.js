import React, {Component} from 'react';
import {Sidebar} from "semantic-ui-react";
import RecipeHiddenSidebar from "../container/menu/RecipeHiddenSidebar";

class PushableView extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Sidebar.Pushable>
                <RecipeHiddenSidebar/>
                <Sidebar.Pusher>
                    {this.props.children}
                </Sidebar.Pusher>
            </Sidebar.Pushable>
        );
    }
}

export default PushableView;
import React from 'react';
import {Menu} from "semantic-ui-react";

const RecipeInstanceItem = ({uid, name, curItem, setCurItem}) => (
    <Menu.Item id={uid} name={name} active={curItem === uid} onClick={() => setCurItem(uid)}>
        {name}
    </Menu.Item>
);

export default RecipeInstanceItem;
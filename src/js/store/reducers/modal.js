import ActionType from "../actions";

const modal = (state = false, action) => {
    switch (action.type) {
        case ActionType.SET_MODAL_STATE:
            return action.state;
        default:
            return state
    }
};

export default modal;
import ActionType from "../actions";

function modRecipeList(state = [], action) {
    switch (action.type) {
        case ActionType.ADD_RECIPE:
            return [
                ...state,
                action.recipe
            ];
        case ActionType.REMOVE_RECIPE:
            return state.filter(o => o.uid !== action.recipe.uid);
        default:
            return state;
    }
}

const recipes = (state = {
    isFetching: false,
    recipes: []
}, action) => {
    switch (action.type) {
        case ActionType.REQUEST_RECIPES:
            return Object.assign({}, state, {
                isFetching: true
            });
        case ActionType.RECEIVE_RECIPES:
            return Object.assign({}, state, {
                isFetching: false,
                recipes: action.recipes
            });
        case ActionType.ADD_RECIPE:
        case ActionType.REMOVE_RECIPE:
            return Object.assign({}, state, {
                recipes: modRecipeList(state.recipes, action)
            });
        default:
            return state;
    }
};

export default recipes;
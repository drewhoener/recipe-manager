import ActionType from "../actions";
import {defaultIngredientError, getDefaultIngredient} from "../actions/form";
import initialState, {evalFrac} from "../constants";
import {units} from '../../components/container/form/IngredientRow';

const validateIngredient = (state = Object.assign({}, initialState.form.ingredients), action) => {
    let values = state.values;
    let errors = [].concat(state.errors);
    if (!action.hasOwnProperty('index') || !values || !values[action.index])
        return state;
    if (errors[action.index] == null)
        errors[action.index] = Object.assign({}, defaultIngredientError);
    let value = values[action.index];
    let error = errors[action.index];
    switch (action.name) {
        case 'name':
            error.name = !/(\S+\s*)+/.test(value.name) ? 'Enter an Ingredient Name' : null;
            break;
        case 'qty':
            error.qty = !(/^\+?(?:(?:\.[1-9]0?|\d+(?:\.\d0?)?)|(?:([1-9]\d*)\s)?([1-9]+)\/([1-9]+))$/.test(value.qty) && evalFrac(value.qty) > 0) ? 'The amount must be greater than 0' : null;
            break;
        case 'unit':
            error.unit = !units.includes(value.unit) ? 'Select a unit of measure' : null;
            break;
        case 'wet':
            error.wet = value.wet < 0 ? 'Select if the ingredient is wet or dry' : null;
            break;
        default:
            return state;
    }
    return {
        values,
        errors
    };
};

const validateInstruction = (state = Object.assign({}, initialState.form.instructions), action) => {
    let values = state.values;
    let errors = [].concat(state.errors);
    if (!values || !action.hasOwnProperty('index'))
        return state;
    errors[action.index] = !/(\S+\s*)+/.test(values[action.index]) ? 'Enter a description for your step' : null;
    return {
        values,
        errors
    };
};

const validateName = (state = '') => {
    return {
        nameError: !/(\S+\s*)+/.test(state) ? 'Enter a name for your recipe' : null
    }
};

const modError = (state = [], action) => {
    let newArr = [];
    switch (action.type) {
        case ActionType.FORM_ADD_INSTRUCTION_ERROR:
        case ActionType.FORM_ADD_INGREDIENT_ERROR:
            newArr = [].concat(state);
            newArr[action.index] = action.error;
            return newArr;
        case ActionType.FORM_REMOVE_INGREDIENT_ERROR:
        case ActionType.FORM_REMOVE_INSTRUCTION_ERROR:
            return state.filter((val, index) => index !== action.index);
        default:
            return state;
    }
};

const modIngredientRow = (state = [], action) => {
    let newArr = [];
    switch (action.type) {
        case ActionType.FORM_ADD_INGREDIENT:
            newArr = [].concat(state);
            newArr[action.index] = action.row;
            return newArr;
        case ActionType.FORM_REMOVE_INGREDIENT:
            return state.filter((val, index) => index !== action.index);
        case ActionType.FORM_CHANGE_INGREDIENT:
            newArr = [].concat(state);
            if (!newArr[action.index])
                newArr[action.index] = getDefaultIngredient();
            newArr[action.index][action.name] = action.val;
            return newArr;
        default:
            return state;
    }
};

const modInstructionRow = (state = [], action) => {
    let newArr = [];
    switch (action.type) {
        case ActionType.FORM_ADD_INSTRUCTION:
            newArr = [].concat(state);
            newArr[action.index] = action.row;
            return newArr;
        case ActionType.FORM_REMOVE_INSTRUCTION:
            return state.filter((val, index) => index !== action.index);
        case ActionType.FORM_CHANGE_INSTRUCTION:
            newArr = [].concat(state);
            if (!newArr[action.index])
                newArr[action.index] = '';
            newArr[action.index] = action.row;
            return newArr;
        default:
            return state;
    }
};

const form = (state = Object.assign({}, initialState.form), action) => {
    switch (action.type) {
        case ActionType.FORM_ADD_INGREDIENT_ERROR:
        case ActionType.FORM_REMOVE_INGREDIENT_ERROR:
            return Object.assign({}, state, {
                ingredients: {
                    values: state.ingredients.values,
                    errors: modError(state.ingredients.errors, action)
                }
            });
        case ActionType.FORM_ADD_INSTRUCTION_ERROR:
        case ActionType.FORM_REMOVE_INSTRUCTION_ERROR:
            return Object.assign({}, state, {
                instructions: {
                    values: state.instructions.values,
                    errors: modError(state.instructions.errors, action)
                }
            });
        case ActionType.FORM_ADD_INGREDIENT:
        case ActionType.FORM_REMOVE_INGREDIENT:
        case ActionType.FORM_CHANGE_INGREDIENT:
            return Object.assign({}, state, {
                ingredients: {
                    values: modIngredientRow(state.ingredients.values, action),
                    errors: state.ingredients.errors
                }
            });
        case ActionType.FORM_ADD_INSTRUCTION:
        case ActionType.FORM_REMOVE_INSTRUCTION:
        case ActionType.FORM_CHANGE_INSTRUCTION:
            return Object.assign({}, state, {
                instructions: {
                    values: modInstructionRow(state.instructions.values, action),
                    errors: state.instructions.errors
                }
            });
        case ActionType.FORM_VALIDATE_INGREDIENT:
            return Object.assign({}, state, {
                ingredients: validateIngredient(state.ingredients, action)
            });
        case ActionType.FORM_VALIDATE_INSTRUCTION:
            return Object.assign({}, state, {
                instructions: validateInstruction(state.instructions, action)
            });
        case ActionType.FORM_CHANGE_RECIPE_NAME:
            return Object.assign({}, state, {name: action.name});
        case ActionType.FORM_VALIDATE_NAME:
            return Object.assign({}, state, validateName(state.name));
        case ActionType.FORM_SET_VALID:
            return Object.assign({}, state, {formValid: !!action.state});
        case ActionType.FORM_SUBMIT_ERROR:
            return Object.assign({}, state, {formError: action.message});
        case ActionType.FORM_RESET:
            return Object.assign({}, initialState.form);
        default:
            return state;
    }
};

export default form;
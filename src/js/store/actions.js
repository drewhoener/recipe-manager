import {setCurrentInstruction} from "./actions/recipes";

const ActionType = {
    REQUEST_RECIPES: 'REQUEST_RECIPES',
    FETCH_RECIPES: 'FETCH_RECIPES',
    RECEIVE_RECIPES: 'RECEIVE_RECIPES',
    ADD_RECIPE: 'ADD_RECIPE',
    REMOVE_RECIPE: 'REMOVE_RECIPE',
    TOGGLE_SIDEBAR: 'TOGGLE_SIDEBAR',
    GET_CURRENT_RECIPE: 'GET_CURRENT_RECIPE',
    SET_CURRENT_RECIPE: 'SET_CURRENT_RECIPE',
    SET_CURRENT_INSTRUCTION: 'SET_CURRENT_INSTRUCTION',
    UPDATE_SCREEN_WIDTH: 'UPDATE_SCREEN_WIDTH',
    FORM_CREATE_REF: 'FORM_CREATE_REF',
    FORM_ADD_INGREDIENT: 'FORM_ADD_INGREDIENT',
    FORM_ADD_INGREDIENT_ERROR: 'FORM_ADD_INGREDIENT_ERROR',
    FORM_ADD_INSTRUCTION: 'FORM_ADD_INSTRUCTION',
    FORM_ADD_INSTRUCTION_ERROR: 'FORM_ADD_INSTRUCTION_ERROR',
    FORM_CHANGE_RECIPE_NAME: 'FORM_CHANGE_RECIPE_NAME',
    FORM_CHANGE_INSTRUCTION: 'FORM_CHANGE_INSTRUCTION',
    FORM_CHANGE_INGREDIENT: 'FORM_CHANGE_INGREDIENT',
    FORM_VALIDATE_NAME: 'FORM_VALIDATE_NAME',
    FORM_VALIDATE_INGREDIENT: 'FORM_VALIDATE_INGREDIENT',
    FORM_VALIDATE_INSTRUCTION: 'FORM_VALIDATE_INSTRUCTION',
    FORM_REMOVE_INGREDIENT: 'FORM_REMOVE_INGREDIENT',
    FORM_REMOVE_INGREDIENT_ERROR: 'FORM_REMOVE_INGREDIENT_ERROR',
    FORM_REMOVE_INSTRUCTION: 'FORM_REMOVE_INSTRUCTION',
    FORM_REMOVE_INSTRUCTION_ERROR: 'FORM_REMOVE_INSTRUCTION_ERROR',
    FORM_SET_VALID: 'SET_FORM_VALID',
    FORM_SUBMIT_RECIPE: 'FORM_SUBMIT_RECIPE',
    FORM_RESET: 'FORM_RESET',
    FORM_SUBMIT_ERROR: 'FORM_SUBMIT_ERROR',
    SET_MODAL_STATE: 'SET_MODAL_STATE',
};

export function addRecipe(recipe){
    return {
        type: ActionType.ADD_RECIPE,
        recipe
    }
}

export function removeRecipe(recipe){
    return {
        type: ActionType.REMOVE_RECIPE,
        recipe
    }
}

export function toggleSidebar(state){
    return {
        type: ActionType.TOGGLE_SIDEBAR,
        state: state
    }
}

export function setCurrentRecipe(recipe){
    return dispatch => {
        dispatch({
            type: ActionType.SET_CURRENT_RECIPE,
            recipe
        });
        dispatch(setCurrentInstruction(0));
    };
}

export function updateScreenWidth(event, data) {
    return {
        type: ActionType.UPDATE_SCREEN_WIDTH,
        width: data.width
    };
}

export default ActionType;

const initialState = {
    curRecipe: '',
    curInstruction: 0,
    recipes: {
        isFetching: false,
        recipes: []
    },
    form: {
        name: '',
        nameError: null,
        refs: [],
        ingredients: {
            values: [],
            errors: []
        },
        instructions: {
            values: [],
            errors: []
        },
        formValid: true,
        formError: ''
    },
    sidebarVisible: false,
    addModalOpen: false,
    width: {
        isMobile: false,
        curWidth: 1000
    }
};

const fracRegex = /(?:([1-9]\d*)\s)?([1-9]+)\/([1-9]+)/;

export function evalFrac(input) {
    if (!isNaN(input)) {
        return parseFloat(input);
    }
    console.log(`Result: ${fracRegex.test(input)}`);
    if (!fracRegex.test(input)) {
        return NaN;
    }
    let match = fracRegex.exec(input);
    console.log(match);
    let result = parseFloat(match[1] ? match[1] : 0) + (parseFloat(match[2]) / parseFloat(match[3]));
    console.log(`Parsed: ${result}`);
    return result;
}

export function evalError(message) {
    return !(message === null || message.length <= 0);
}

export default initialState;
import ActionType from "../actions";

const axios = require("axios");

export function getDefaultRecipe() {
    return {
        uid: '',
        name: '',
        ingredients: [],
        instructions: []
    }
}

export function setCurrentInstruction(instruction) {
    return {
        type: ActionType.SET_CURRENT_INSTRUCTION,
        instruction
    }
}

export function requestRecipes() {
    return {
        type: ActionType.REQUEST_RECIPES
    }
}

export function fetchRecipes() {
    return dispatch => {
        dispatch(requestRecipes());
        return axios.get(`/api/recipes`)
            .then(response => response.data,
                // Do not use catch, because that will also catch
                // any errors in the dispatch and resulting render,
                // causing a loop of 'Unexpected batch number' errors.
                // https://github.com/facebook/react/issues/6895
                error => console.log('An error occurred.', error)
            )
            .then(json => dispatch(receiveRecipes(json)))
    }
}

export function receiveRecipes(json) {
    return {
        type: ActionType.RECEIVE_RECIPES,
        recipes: json.recipes
    }
}

export function shouldFetchRecipes(state) {
    const recipes = state.recipes;
    console.log(`State:`);
    console.log(state);
    console.log(`State: ${!recipes || !recipes.isFetching}`);
    return !recipes || !recipes.isFetching;
}

export function fetchRecipesIfNeeded() {
    // Note that the function also receives getState()
    // which lets you choose what to dispatch next.

    // This is useful for avoiding a network request if
    // a cached value is already available.

    return (dispatch, getState) => {
        if (shouldFetchRecipes(getState())) {
            // Dispatch a thunk from thunk!
            console.log('Fetching Recipes');
            return dispatch(fetchRecipes())
        } else {
            // Let the calling code know there's nothing to wait for.
            return Promise.resolve()
        }
    }
}
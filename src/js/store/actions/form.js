import ActionType, {addRecipe} from "../actions";

const axios = require('axios');
const React = require("react");

const defaultIngredient = {
    name: '',
    qty: '0',
    unit: '--',
    wet: -1
};

const defaultIngredientError = {
    name: null,
    qty: null,
    unit: null,
    wet: null
};

export function getDefaultIngredient() {
    return Object.assign({}, defaultIngredient);
}

export function changeName(name) {
    return {
        type: ActionType.FORM_CHANGE_RECIPE_NAME,
        name
    }
}

export function validateName() {
    return {
        type: ActionType.FORM_VALIDATE_NAME
    }
}

export function changeAndValidateName(name) {
    return dispatch => {
        dispatch(changeName(name));
        dispatch(validateName());
    }
}

export function addIngredientRow(event, index) {
    if (event)
        event.preventDefault();
    const row = Object.assign({}, defaultIngredient);
    const error = Object.assign({}, defaultIngredientError);
    return dispatch => {
        dispatch({
            type: ActionType.FORM_ADD_INGREDIENT,
            index,
            row
        });
        dispatch({
            type: ActionType.FORM_ADD_INGREDIENT_ERROR,
            index,
            error
        })
    };
}

export function addInstructionRow(event, index) {
    if (event)
        event.preventDefault();
    const row = '';
    return dispatch => {
        dispatch({
            type: ActionType.FORM_ADD_INSTRUCTION,
            index,
            row
        });
        dispatch({
            type: ActionType.FORM_ADD_INSTRUCTION_ERROR,
            index,
            error: null
        })
    };
}

export function changeAndValidateIngredientRow(index, name, val) {
    return dispatch => {
        dispatch(changeIngredientRow(index, name, val));
        dispatch(validateIngredientRow(index, name));
    }
}

export function changeAndValidateInstructionRow(index, name, val) {
    return dispatch => {
        dispatch(changeInstructionRow(index, name, val));
        dispatch(validateInstructionRow(index));
    }
}

export function validateIngredientRow(index, name) {
    return {
        type: ActionType.FORM_VALIDATE_INGREDIENT,
        index,
        name
    };
}

export function validateInstructionRow(index) {
    return {
        type: ActionType.FORM_VALIDATE_INSTRUCTION,
        index
    };
}

export function changeIngredientRow(index, name, val) {
    return {
        type: ActionType.FORM_CHANGE_INGREDIENT,
        index,
        name,
        val
    }
}

export function changeInstructionRow(index, row) {
    return {
        type: ActionType.FORM_CHANGE_INSTRUCTION,
        index,
        row
    }
}

export function removeIngredientRow(event, index) {
    return dispatch => {
        dispatch({
            type: ActionType.FORM_REMOVE_INGREDIENT,
            index
        });
        dispatch({
            type: ActionType.FORM_REMOVE_INGREDIENT_ERROR,
            index
        });
    };
}

export function removeInstructionRow(event, index) {
    return dispatch => {
        dispatch({
            type: ActionType.FORM_REMOVE_INSTRUCTION,
            index
        });
        dispatch({
            type: ActionType.FORM_REMOVE_INSTRUCTION_ERROR,
            index
        });
    };
}

export function setFormValid(state) {
    return {
        type: ActionType.FORM_SET_VALID,
        state
    }
}

export function resetForm() {
    return {
        type: ActionType.FORM_RESET
    }
}

export function formSubmitError(message) {
    return {
        type: ActionType.FORM_SUBMIT_ERROR,
        message
    }
}

export function formValidateRecipe() {
    return (dispatch, getState) => {
        const ingredients = getState().form.ingredients.values;
        const instructions = getState().form.instructions.values;
        let valid = true;
        dispatch(validateName());
        valid &= (getState().form.nameError === null);
        for (let index in ingredients) {
            for (let key in ingredients[index]) {
                if (!ingredients[index].hasOwnProperty(key))
                    continue;
                dispatch(validateIngredientRow(index, key));
                let bool = getState().form.ingredients.errors[index][key] === null;
                //console.log(`Value for ${ingredients[index][key]}: ${bool}`);
                valid &= bool;
            }
        }
        for (let index in instructions) {
            dispatch(validateInstructionRow(index));
            let bool = getState().form.instructions.errors[index] === null;
            //console.log(`Value for ${instructions[index]}: ${bool}`);
            valid &= bool;
        }
        dispatch(setFormValid(valid));
    }
}

export function formSubmitRecipe(event) {
    event.preventDefault();
    return (dispatch, getState) => {
        let form = {name: `${getState().form.name}`};
        form.ingredients = [].concat(getState().form.ingredients.values);
        form.instructions = [].concat(getState().form.instructions.values);
        dispatch(formValidateRecipe());
        if (!getState().form.formValid) {
            dispatch(formSubmitError('There are unfilled rows. Complete or remove your empty rows before submitting.'));
            return;
        }
        axios.post('/api/add_recipe', form)
            .then(response => {
                dispatch(addRecipe(response.data.recipe));
            })
            .catch(error => dispatch(formSubmitError(error.message)));
        dispatch(resetForm());
    }
}

export {defaultIngredientError};
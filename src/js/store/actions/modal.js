import ActionType from "../actions";

export function setModalState(e, state) {
    e.preventDefault();
    return {
        type: ActionType.SET_MODAL_STATE,
        state
    }
}
import ActionType from "./actions";
import {combineReducers} from "redux";
import {Responsive} from "semantic-ui-react";
import recipes from "./reducers/recipes";
import form from "./reducers/form";
import modal from "./reducers/modal";

const widths = {
    minWidth: Responsive.onlyMobile.minWidth,
    maxWidth: Responsive.onlyTablet.maxWidth
};

function modCurRecipe(state = '', action){
    switch(action.type){
        case ActionType.SET_CURRENT_RECIPE:
            return action.recipe;
        default:
            return state;
    }
}

function modCurInstruction(state = 0, action) {
    switch (action.type) {
        case ActionType.SET_CURRENT_INSTRUCTION:
            return action.instruction;
        default:
            return state;
    }
}

function modSidebarVisible(state = false, action){
    switch(action.type){
        case ActionType.TOGGLE_SIDEBAR:
            return action.state;
        default:
            return state;
    }
}

function modWidth(state = false, action) {
    let width = action.width;
    const {minWidth, maxWidth} = widths;
    switch (action.type) {
        case ActionType.UPDATE_SCREEN_WIDTH:
            width = action.width;
            break;
        default:
            width = window.innerWidth;
            break;
    }
    return {
        isMobile: (minWidth ? width >= minWidth : true) && (maxWidth ? width <= maxWidth : true),
        curWidth: width
    };
}

const modState = combineReducers({
    curRecipe: modCurRecipe,
    curInstruction: modCurInstruction,
    recipes: recipes,
    form: form,
    sidebarVisible: modSidebarVisible,
    addModalOpen: modal,
    width: modWidth
});

export default modState;

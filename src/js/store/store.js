import modState from "./reducers";
import {applyMiddleware, createStore} from "redux";
import thunkMiddleware from 'redux-thunk';
import initialState from "./constants";

const store = createStore(modState, initialState, applyMiddleware(thunkMiddleware));
//const unsubscribe = store.subscribe(() => console.log(store.getState()));

export {store};
import logger from './js/logging';

const util = require('util');
const log = logger.extend('db');
const MongoClient = require('mongodb').MongoClient;
const DBUrl = 'mongodb://%slocalhost/';

let db = null;
let client = null;

function adapt_recipe_obj(o) {
    let obj = {
        uid: o._id,
        name: o.name,
        instructions: o.instructions
    };
    obj.ingredients = o.ingredients.map(val => {
        return {
            name: val.name,
            qty: val.qty,
            wet: !!val.wet,
            unit: val.unit
        }
    });
    return obj;
}

export function add_recipe(recipe) {
    let collection = db.collection('recipes');
    return collection.insertOne(recipe)
        .then(result => {
            if (result.result.ok !== 1 || !result.ops.length) {
                log(result.result.ok);
                log(result.ops.length);
                log('Throwing an error');
                throw new Error('Could not set database object');
            }
            return adapt_recipe_obj(result.ops[0]);
        })
}

export function get_recipes() {
    let collection = db.collection('recipes');
    return collection.find({}).toArray()
        .then(arr => {
            return arr.map(o => adapt_recipe_obj(o));
        })
        .catch(console.error);
}

export function connect(user, pass) {
    let auth = '';
    if (user && pass) {
        auth = `${user}:${pass}@`;
    }
    MongoClient.connect(util.format(DBUrl, auth), {useNewUrlParser: true}).then((db_client) => {
        client = db_client;
        db = client.db('recipe_manager');
        log('Database connected.');
        //db.listCollections().toArray((err, colls) => log(JSON.stringify(colls, null, 4)));

    }).catch((err) => log('Error Thrown: ' + JSON.stringify(err, null, 4)));

    return db;
}

export function close() {
    client.close();
}

export {client, db};

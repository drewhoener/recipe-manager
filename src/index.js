import React from 'react';
import {Provider} from 'react-redux';
import {render} from 'react-dom';
import {store} from "./js/store/store";
import RecipeMainView from "./js/components/presentation/RecipeMainView";
import {fetchRecipes} from "./js/store/actions/recipes";
// noinspection ES6UnusedImports
import style from './css/style.css';

store
    .dispatch(fetchRecipes())
    .then(() => console.log(store.getState()));

const entryPoint = document.getElementById('react-alt-entry');
if (entryPoint) {
    render(
        <Provider store={store}>
            <RecipeMainView/>
        </Provider>
        , entryPoint
    );
}
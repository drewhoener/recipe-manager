const path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: "/"
    },
    mode: 'development',
    devtool: "inline-source-map",
    stats: {
        assets: false,
        modules: false,
        chunks: false,
        children: false,
        hash: false,
        entrypoints: false,
        version: false,
    },
    module: {
        rules: [
            {
                test: /\.m?js(x)?$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        babelrc: true
                    }
                }
            },
            {
                test: /\.(woff(2)?|otf|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: './font/[name].[ext]',
                        },
                    },
                ]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Recipe Manager',
            template: "./src/index.ejs",
            filename: "./index.html",
            react_id: 'react-entry'
        }),
        new HtmlWebpackPlugin({
            title: 'Recipe Manager',
            template: "./src/alt.ejs",
            filename: "./alt.html",
            react_id: 'react-alt-entry'
        }),
        /*
        new CompressionPlugin({
            filename: '[path].gz[query]',
            test: /\.(js|css|html|svg)$/,
            compressionOptions: {level: 2},
            threshold: 10240,
            minRatio: 0.8,
            deleteOriginalAssets: true,
        })*/
    ]
};
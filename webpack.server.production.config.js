const path = require('path');
const webpack = require('webpack');
const WebpackNodeExternals = require('webpack-node-externals');
const TerserPlugin = require('terser-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: './src/servers/server.js',
    output: {
        filename: 'server.js',
        publicPath: '/',
        path: path.resolve(__dirname, 'dist')
    },
    mode: 'production',
    target: "node",
    node: {
        __dirname: false,
        __filename: false
    },
    //Express also needs this?
    externals: [WebpackNodeExternals()],
    /*
    stats: {
        assets: false,
        modules: false,
        chunks: false,
        children: false,
        hash: false,
        entrypoints: false,
        version: false,
    },*/
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        babelrc: true
                    }
                }
            }
        ]
    },
    optimization: {
        minimizer: [new TerserPlugin()],
    },
    plugins: [
        new webpack.DefinePlugin({ // <-- key to reducing React's size
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.HashedModuleIdsPlugin(),
        //new BundleAnalyzerPlugin(),
    ]
};
const path = require('path');
const webpack = require("webpack");
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    entry: {
        index: `./src/index.js`,
    },
    output: {
        filename: '[name].[contenthash].bundle.js',
        chunkFilename: "[name].[contenthash].bundle.js",
        path: path.resolve(__dirname, 'dist'),
        publicPath: "/"
    },

    mode: 'production',
    /*
    stats: {
        assets: false,
        modules: false,
        chunks: false,
        children: false,
        hash: false,
        entrypoints: false,
        version: false,
    },*/
    module: {
        rules: [
            {
                test: /\.m?js(x)?$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        babelrc: true
                    }

                }
            },
            {
                test: /\.(woff(2)?|otf|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: './font/[name].[ext]',
                        },
                    },
                ]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    optimization: {
        minimizer: [new TerserPlugin()],
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    enforce: true,
                    chunks: 'all'
                }
            }
        }
    },
    plugins: [
        new webpack.DefinePlugin({ // <-- key to reducing React's size
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks
        new CleanWebpackPlugin(),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new HtmlWebpackPlugin({
            title: 'Recipe Manager',
            template: "./src/index.ejs",
            filename: "./index.html",
            react_id: 'react-entry'
        }),
        new HtmlWebpackPlugin({
            title: 'Recipe Manager',
            template: "./src/alt.ejs",
            filename: "./alt.html",
            react_id: 'react-alt-entry'
        }),
        new webpack.HashedModuleIdsPlugin(),
        new CompressionPlugin({
            filename: '[path].br[query]',
            algorithm: 'brotliCompress',
            test: /\.(js|css|html|svg)$/,
            compressionOptions: {level: 11},
            threshold: 10240,
            minRatio: 0.8,
            deleteOriginalAssets: true,
        }),
        //new BundleAnalyzerPlugin(),
    ]
};
import {to_metric_str} from "../src/js/conversions";
import {evalFrac} from "../src/js/store/constants";

test('Tests applied regex', () => {
    to_metric_str('15 cups of sugar sifted but not too much');
    console.log(evalFrac('100 2/3'));
});
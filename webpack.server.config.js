const path = require('path');
const WebpackNodeExternals = require('webpack-node-externals');

module.exports = {
    entry: './src/servers/server.js',
    output: {
        filename: 'server.js',
        publicPath: '/',
        path: path.resolve(__dirname, 'dist')
    },
    mode: 'development',
    devtool: "inline-source-map",
    target: "node",
    node: {
        __dirname: false,
        __filename: false
    },
    //Express also needs this?
    externals: [WebpackNodeExternals()],
    stats: {
        assets: false,
        modules: false,
        chunks: false,
        children: false,
        hash: false,
        entrypoints: false,
        version: false,
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            [
                                "@babel/preset-env",
                                {
                                    "targets": {
                                        "node": "current"
                                    }
                                }
                            ]
                        ],
                        //plugins: ['@babel/plugin-proposal-object-rest-spread']
                    }
                }
            }
        ]
    }
};
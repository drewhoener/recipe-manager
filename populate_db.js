db = new Mongo().getDB('recipe_manager');

db.locations.remove({});

db.recipes.update({name: 'Pound Cake'}, {
    "$setOnInsert":
        {
            name: 'Pound Cake',
            ingredients: [
                {
                    "name": "Flour",
                    "wet": false,
                    "qty": 1,
                    "unit": "c"
                },
                {
                    "name": "Egg",
                    "wet": true,
                    "qty": 2,
                    "unit": "whole"
                }
            ],
            instructions: [
                'Add Egg to Flour',
                'Cry'
            ]
        }
}, {upsert: true});

db.recipes.update({name: 'Mac and Cheese'}, {
    "$setOnInsert":
        {
            name: 'Mac and Cheese',
            ingredients: [
                {
                    "name": "Butter",
                    "wet": true,
                    "qty": 5,
                    "unit": "Tbsp"
                },
                {
                    "name": "Flour",
                    "wet": false,
                    "qty": '1/4',
                    "unit": "cup"
                },
                {
                    "name": "Milk",
                    "wet": true,
                    "qty": '4',
                    "unit": "cups"
                }
            ],
            instructions: [
                'Melt butter in medium saucepan',
                'Add flour and whisk over medium heat for 3 minutes',
                'In a slow stream, add milk, whisking constantly'
            ]
        }
}, {upsert: true});